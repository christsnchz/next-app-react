import * as React from 'react';
import Layout from '../components/layouts/Layout';

export interface NuevoProductoProps {
  
}
 
const NuevoProducto: React.FC<NuevoProductoProps> = () => {
  return (
    <Layout>
      <h1>Nuevo producto</h1>
    </Layout>  
  
  );
}
 
export default NuevoProducto;