import styled from '@emotion/styled';
import * as React from 'react';
import Layout from '../components/layouts/Layout';
import { Campo, Formulario, InputSubmit } from '../components/ui/Formulario';

export interface CrearCuentaProps {
  
}

const Titulo = styled.h1`
  text-align: center;
  margin-top: 5rem;
`;
 
const CrearCuenta: React.FC<CrearCuentaProps> = () => {
  return ( 
    <Layout>
      <React.Fragment>
        <Titulo>crear cuenta</Titulo>

        <Formulario action="">
          <Campo>
            <label htmlFor="nombre"> Nombre</label>
            <input type="text"
              id="nombre"
              placeholder="tu nombre"
              name="nombre"
            />
          </Campo>

          <Campo>
            <label htmlFor="email"> Email</label>
            <input type="email"
              id="email"
              placeholder="tu email"
              name="email"
            />
          </Campo>

          <Campo>
            <label htmlFor="password"> password</label>
            <input type="password"
              id="password"
              placeholder="tu password"
              name="password"
            />
          </Campo>

          <InputSubmit type="submit"
            value="Crear Cuenta" />
          
        </Formulario>
      </React.Fragment>
    </Layout>
   );
}
 
export default CrearCuenta;