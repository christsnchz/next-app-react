import * as React from 'react';
import Layout from '../components/layouts/Layout';

export interface PopularesProps {
  
}
 
const Populares: React.FC<PopularesProps> = () => {
  return (
    <Layout>
      <h1>Populares</h1>
    </Layout>
  );
}
 
export default Populares;