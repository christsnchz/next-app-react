import * as React from 'react';

export interface useValdiacionProps {
  stateInicial: any;
  validar: any;
  fn: any;
}
 
const useValdiacion: React.FC<useValdiacionProps> = ({stateInicial, fn}) => {

  const [valores, guardarValores] = React.useState(stateInicial);
  const [errores, guardarErrores] = React.useState({});
  const [submitForm, guardarSubmitForm] = React.useState(false);

  React.useEffect(() => {

    if (submitForm) {
      const noErrores = Object.keys(errores).length === 0;

      if (noErrores) {
        fn();
      }

      guardarSubmitForm(false);
    }

  }, []);


  return ( <h1>h</h1> );
}
 
export default useValdiacion;