import * as React from 'react';
import Buscar from '../ui/Buscar';
import Navegacion from './Navegacion';
import Link from 'next/link';
import styled from '@emotion/styled';
import Boton from '../ui/Boton';

export interface HeaderProps {
  
}

const ContenedorHeader = styled.div`
  max-width: 1200px;
  width: 95%;
  margin: 0 auto;
  @media (min-width:768px) {
    display: flex;
    justify-content: space-between;
  }
`;

const Logo = styled.p`
  color: #DA552F;
  font-size: 4rem;
  line-height: 0;
  font-weight: 700;
  font-family: serif;
  margin-right: 2rem;
`;

const Div = styled.div`
display:flex;
align-items: center;
`;

const Headers = styled.div`
border-bottom: 2px solid #e1e1e1;
padding: 1rem 0;
`;

const P = styled.p`
  margin-right: 2rem;
`; 
 
const Header: React.FC<HeaderProps> = () => {

  const usuario = false;

  return ( 
    <Headers>
      <ContenedorHeader>
        <Div>
          <Link href="/">
            <Logo>P</Logo>
          </Link>

          <Buscar />
            
          <Navegacion/>
        </Div>

        <Div>
          {usuario ? (
            <React.Fragment>
            <P>Hola: Christian</P>
  
              <Boton bgColor="true" > Cerrar Sesion </Boton>
            </React.Fragment>
           
          ) : (
            <React.Fragment>
              <Link href="/login">
                <Boton bgColor="true">Login</Boton> 
              </Link>
              <Link href="/crear-cuenta">
                <Boton>Crear Cuenta</Boton>
              </Link>
            </React.Fragment>              
         )}

        </Div>
      </ContenedorHeader>
    </Headers>

   );
}
 
export default Header;