import styled from '@emotion/styled';

const Boton = styled.a`
  font-weight: 700;
  text-transform: uppercase;
  border: 1px solid #d1d1d1;
  padding: .8rem 2rem;
  font-size: 13px;
  margin-right: 1rem;
  background-color: ${(props : any) => props.bgColor ? '#DA552F' : 'white'};
  color: ${(props : any) => props.bgColor ? '#FFFF' : '#000'};

    &:last-of.type {
      margin-right:0;
    }

    
`;

export default Boton;