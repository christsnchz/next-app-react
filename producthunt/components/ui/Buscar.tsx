import * as React from 'react';
import styled from '@emotion/styled';

const InputText = styled.input`
  border: 1px solid var(--gris3);
  padding: 1rem;
  min-width: 300px;
`;

const InputSubmit = styled.button`
  height: 3rem;
  width: 3rem;
  display: block;
  background-size: 4rem;
  background-repeat: no-repeat;
  position: absolute;
  right: 1rem;
  top: 1px;
  background-color: white;
  border: none;  
`;

const Form = styled.form`
  position: relative;
`;

export interface BuscarProps {
  
}
 
const Buscar: React.FC<BuscarProps> = () => {
  return (  

    <Form>
      <InputText
        placeholder="buscar productos"
        type="text" />

      <InputSubmit type="submit" >B</InputSubmit>
    </Form>
  );
}
 
export default Buscar;